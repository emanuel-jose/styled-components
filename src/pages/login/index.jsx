import { Container, CardContainer } from "./style"
import Header from "../../components/header"
import Form from "../../components/form"
import CardLinks from "../../components/card-links"
import Footer from "../../components/footer"

const links = {
  product: [
    "Next.js",
    "Vercel for GitHub",
    "Vercel for GitLab",
    "Vercel for Bitbucket",
    "Integrations",
    "Command-Line",
    "Edge Network",
  ],
  resources: [
    "Documentation",
    "Guides",
    "Knowledge",
    "Blog",
    "API Reference",
    "Examples",
    "OSS",
    "Partners",
    "Support",
  ],
  company: [
    "Home",
    "About",
    "Careers",
    "Pricing",
    "Security",
    "backendlessConf_",
    "Contact Us",
  ],
  legal: [
    "Privacy Policy",
    "Terms of Service",
    "Trademark Policy",
    "Inactivity Policy",
    "DPA",
    "SLA",
  ],
}

const Login = () => {
  const {product, resources, company, legal} = links
  return (
    <Container>
      <Header />
      <Form />
      <CardContainer>
        <CardLinks title="Product" links={product}/>
        <CardLinks title="Resources" links={resources}/>
        <CardLinks title="Company" links={company}/>
        <CardLinks title="Legal" links={legal}/>
      </CardContainer>
      <Footer />
    </Container>
  )
}

export default Login
