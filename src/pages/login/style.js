import styled from 'styled-components'

export const Container = styled.div`
    width: 50vw;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const CardContainer = styled.div`
    width: 100%;
    margin-top: 20px;
    margin-bottom: 100px;
    display: flex;
    justify-content: space-between;
    align-items: center;

`