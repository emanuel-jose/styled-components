import styled, { css } from 'styled-components'

export const Container = styled.div`
    width: 400px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items:center;
    margin: 130px 0;

    p {
        margin-bottom: 300px;
        color: #1f1f1f;
    }

    a {
        text-decoration: none;
        color: rgb(0, 112, 243);
    }

    h1 {
        font-size: 50px;
        margin-top: 100px;
    }
`

export const Button = styled.button`
    width: 80%;
    margin: 10px;
    padding: 15px;
    border: none;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    color: white;
    font-weight: 700;
    border-radius: 5px;

    ${({color}) => {
        if (color) {
            return css`
                background-color: #${color};
                transition: .2s;

                :hover {
                    cursor: pointer;
                    background-color: #${color}90;
                }
            `
        }
    }}

    i {
        font-size: 20px;
    }

`