import { Container, Button } from "./style"

const Form = () => {
  return (
    <Container>
      <h1>Log in to Vercel</h1>
      
        <Button color="000000"><i className="fab fa-github"></i>Continue with GitHub</Button>
        <Button color="6b4fbb"><i className="fab fa-gitlab"></i>Continue with GitLab</Button>
        <Button color="1668E2"><i className="fab fa-bitbucket"></i>Continue with Bitbucked</Button>
      
      <p>
        You can also <a href="/">continue with email</a>
      </p>
      <a href="/">Don't have an account? Sign Up</a>
    </Container>
  )
}

export default Form
