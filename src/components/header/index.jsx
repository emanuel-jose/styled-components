import {Container, ImgContainer, ButtonContainer, Button} from './style'
import logo from '../../img/logo.svg'

const Header = () => {
  return (
    <Container>
      <ImgContainer>
        <img src={logo} alt="logo" />
      </ImgContainer>
      <ButtonContainer>
        <Button primary>Contact</Button>
        <Button primary>Login</Button>
        <Button secondary>Sign Up</Button>
      </ButtonContainer>
    </Container>
  )
}

export default Header
