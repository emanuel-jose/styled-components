import styled, { css } from "styled-components"

export const Container = styled.div`
  width: 60vw;
  padding-top: 10px;
  display: flex;
  justify-content: space-between;
  background-color: #ffffff97;
  position: fixed;
`

export const ImgContainer = styled.div`
  width: 120px;
  img {
    width: 100%;
  }
`

export const ButtonContainer = styled.div`
  width: 225px;
  display: flex;
  justify-content: space-between;
`

export const Button = styled.button`
  border: 1px solid white;
  box-sizing: border-box;
  padding: 5px 10px;
  ${({ primary, secondary }) => {
    if (primary) {
      return css`
        color: gray;
        transition: 0.2s;
        :hover {
          cursor: pointer;
          color: black;
        }
      `
    }

    if (secondary) {
      return css`
        background-color: black;
        color: white;
        border-radius: 5px;
        transition: 0.2s;

        :hover {
          cursor: pointer;
          background-color: white;
          color: black;
          border-color: black;
        }
      `
    }
  }}
`
