import styled from 'styled-components'

export const Container = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    padding-bottom: 30px;
`

export const CompanyInfo = styled.div`
    display:flex;
    flex-direction: column;
    
    img {
        width: 90px;
    }

    p {
        margin: 20px 0 0;
        padding: 0;
        color: gray;
        font-size: 15px;
    }
`

export const Social = styled.div`
    width: 60px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    i {
        font-size: 20px;
        color: gray;
        transition: .2s
    }

    i:hover {
        cursor: pointer;
        color: black;
    }
`

export const Status = styled.div`
    font-size: 15px;

    span {
        font-weight: 500;
        color: rgb(0, 112, 243);
    }

    i {
        color: rgb(0, 112, 243);
        font-size: 10px;
    }
`

export const Mode = styled.div`
    width: 100px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    select {
        border: none;
        padding: 5px;
        width: 80px;
        appearance: none;
    }
`