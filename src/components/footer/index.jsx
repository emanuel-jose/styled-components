import { Container, CompanyInfo, Social, Status, Mode } from "./style"
import logo from "../../img/logo.svg"
import {useState} from 'react'

const Footer = () => {
    const [mode, setMode] = useState('sun')

    const modifyIcon = (value) => {
        if (value === 'light') {
            setMode('sun')
        }

        if (value === 'dark') {
            setMode('moon')
        }
    }
  return (
    <Container>
      <CompanyInfo>
        <img src={logo} alt="logo" />
        <p>Copyright &copy; 2020 Vercel Inc. All rights reserved.</p>
      </CompanyInfo>
      <Social>
        <i className="fab fa-github"></i>
        <i className="fab fa-twitter"></i>
      </Social>
      <Status>Status: <i className="fas fa-circle"></i> <span>All systems norm...</span></Status>
      <Mode>
      <i className={`far fa-${mode}`}></i>
        <select name="mode">
          <option value="light" onClick={(ev) => {
              modifyIcon(ev.target.value)
          }}>
           Light
          </option>
          <option value="dark" onClick={(ev) => {
              modifyIcon(ev.target.value)
          }}>Dark</option>
        </select>
      </Mode>
    </Container>
  )
}

export default Footer
