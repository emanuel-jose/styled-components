import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction:column;
    width: 130px;
    height: 300px;

    h3 {
        font-size: 14px;
        font-weight: 400;
    }

    a {
        font-size: 14px;
        display: inline-block;
        text-decoration: none;
        color: gray;
        margin-bottom: 16px;
        transition: .2s;
    }

    a:hover {
        color: black;
    }
`