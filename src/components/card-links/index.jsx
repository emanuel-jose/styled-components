import {Container} from './style'

const CardLinks = ({ title, links }) => {
  return (
    <Container>
      <h3>{title}</h3>
      {links.map((link, index) => (
        <a key={index} href="/">
          {link}
        </a>
      ))}
    </Container>
  )
}

export default CardLinks
